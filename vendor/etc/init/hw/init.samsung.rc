# Copyright (c) 2015-2021, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Changes from Qualcomm Innovation Center are provided under the following license:
#
# Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#
#

import /vendor/etc/init/hw/init.samsung.bsp.rc

on early-init
    mkdir /mnt/vendor/efs 0771 radio system
#    mkdir /mnt/vendor/persist 0771 root system

on init
    symlink /dev/block/by-name/steady /dev/block/steady
    symlink /dev/block/by-name/persistent /dev/block/persistent

# Create carrier folder for HiddenMenu
on post-fs
    mkdir /efs/carrier 0755 system system
    restorecon_recursive /efs

    restorecon_recursive /mnt/vendor/efs
    chown radio system /mnt/vendor/efs
    chmod 0771 /mnt/vendor/efs

    chown system radio /efs
    chmod 0771 /efs
    restorecon /efs

on post-fs-data
    # carrier partition
#    chown system system /carrier
#    chmod 0771 /carrier
#    restorecon /carrier

    # Permissions for Charging
    mkdir /efs/Battery 0775 radio system
    chown radio system /efs/Battery
    chmod 0775 /efs/Battery
    chown system system /data/vendor/log/chg
    chmod 0775 /data/vendor/log/chg
    chown system system /data/vendor/log/chg/battery_dump1
    chown system system /data/vendor/log/chg/battery_dump2
    copy /system/vendor/firmware/battery_data.dat /efs/Battery/battery_data.dat
    chmod 0400 /efs/Battery/battery_data.dat
    write /sys/class/power_supply/battery/batt_update_data "/efs/Battery/battery_data.dat"
    chown system radio /sys/class/power_supply/battery/batt_ext_dev_chg
    chown system radio /sys/class/power_supply/battery/batt_temp_control_test
    chown system radio /efs/Battery/fg_full_voltage
    chown system radio /sys/class/power_supply/battery/fg_full_voltage
    chown system radio /sys/class/power_supply/battery/batt_self_discharging_control
    chown system radio /sys/class/power_supply/battery/batt_inbat_wireless_cs100
    chmod 0664 /efs/Battery/fg_full_voltage
    chmod 0664 /sys/class/power_supply/battery/fg_full_voltage
    chown system radio /sys/class/power_supply/battery/test_mode
    chown system radio /sys/class/power_supply/battery/batt_reset_soc
    chown system radio /sys/class/power_supply/battery/batt_slate_mode
    chown system radio /sys/class/power_supply/battery/batt_swelling_control
    chown system radio /sys/class/power_supply/battery/batt_battery_id
    chown system radio /sys/class/power_supply/battery/batt_sub_battery_id
    chown system radio /sys/class/power_supply/battery/factory_mode
    chown system radio /sys/class/power_supply/battery/siop_level
    chown system radio /sys/class/power_supply/battery/siop_event
    chown system radio /sys/class/power_supply/battery/batt_temp
    chown system radio /sys/class/power_supply/battery/wc_enable
    chown system radio /sys/class/power_supply/battery/factory_mode_relieve
    chown system radio /sys/class/power_supply/battery/factory_mode_bypass
    chown system radio /sys/class/power_supply/battery/normal_mode_bypass
    chown system radio /sys/class/power_supply/battery/factory_voltage_regulation
    chown system radio /sys/class/power_supply/battery/factory_mode_disable
    chown system radio /sys/class/power_supply/battery/batt_pon_ocv
    chown system radio /sys/class/power_supply/battery/batt_user_pon_ocv
    chown system radio /sys/class/power_supply/battery/set_ship_mode
    chmod 0664 /sys/class/power_supply/battery/set_ship_mode
    chown system nfc /sys/class/power_supply/battery/wc_control
    chown system nfc /sys/class/power_supply/battery/wc_control_cnt
    chown system nfc /sys/class/power_supply/battery/led_cover
    chown system radio /sys/class/power_supply/battery/update
    chown system radio /sys/class/power_supply/battery/batt_wdt_control
    chown system sdcard_rw /sys/class/power_supply/battery/store_mode
    chown sdcard_rw sdcard_rw /sys/class/power_supply/battery/call
    chown sdcard_rw sdcard_rw /sys/class/power_supply/battery/video
    chown sdcard_rw sdcard_rw /sys/class/power_supply/battery/music
    chown sdcard_rw sdcard_rw /sys/class/power_supply/battery/browser
    chown sdcard_rw sdcard_rw /sys/class/power_supply/battery/hotspot
    chown sdcard_rw sdcard_rw /sys/class/power_supply/battery/camera
    chown system radio /sys/class/power_supply/battery/talk_wcdma
    chown system radio /sys/class/power_supply/battery/talk_gsm
    chown system radio /sys/class/power_supply/battery/call
    chown system radio /sys/class/power_supply/battery/data_call
    chown system radio /sys/class/power_supply/battery/gps
    chown system radio /sys/class/power_supply/battery/wifi
    chown system radio /sys/class/power_supply/battery/lte
    chown system radio /sys/class/power_supply/battery/lcd
    chown system radio /sys/class/power_supply/ps/status
    chmod 0664 /sys/class/power_supply/battery/wc_control
    chmod 0664 /sys/class/power_supply/battery/wc_control_cnt
    chmod 0664 /sys/class/power_supply/battery/led_cover
    chmod 0664 /sys/class/power_supply/ps/status
    chmod 0664 /sys/class/power_supply/battery/batt_temp_table
    chown system radio /sys/class/power_supply/battery/batt_temp_table
    chown system radio /sys/class/power_supply/battery/batt_high_current_usb
    chown system radio /sys/class/power_supply/battery/batt_inbat_voltage
    chmod 0664 /sys/class/power_supply/battery/batt_high_current_usb
    chown system radio /sys/class/power_supply/battery/hmt_ta_connected
    chown system radio /sys/class/power_supply/battery/hmt_ta_charge
    chown system radio /sys/class/power_supply/battery/battery_cycle
    chown system radio /sys/class/power_supply/otg/online
    chmod 0664 /sys/class/power_supply/otg/online
    chown system radio /sys/class/power_supply/battery/charge_otg_control
    chown system radio /sys/class/power_supply/battery/charge_uno_control
    chown system radio /sys/class/power_supply/battery/batt_wireless_firmware_update
    chown system radio /sys/class/power_supply/battery/otp_firmware_result
    chown system radio /sys/class/power_supply/battery/wc_ic_grade
    chown system radio /sys/class/power_supply/battery/wc_ic_chip_id
    chown system radio /sys/class/power_supply/battery/otp_firmware_ver_bin
    chown system radio /sys/class/power_supply/battery/otp_firmware_ver
    chown system radio /sys/class/power_supply/battery/tx_firmware_result
    chown system radio /sys/class/power_supply/battery/batt_tx_firmware
    chown system radio /sys/class/power_supply/battery/batt_hv_wireless_status
    chown system radio /sys/class/power_supply/battery/batt_hv_wireless_pad_ctrl
    chmod 0664 /sys/class/power_supply/battery/charge_otg_control
    chmod 0664 /sys/class/power_supply/battery/charge_uno_control
    chmod 0664 /sys/class/power_supply/battery/batt_wireless_firmware_update
    chmod 0664 /sys/class/power_supply/battery/otp_firmware_result
    chmod 0664 /sys/class/power_supply/battery/wc_ic_grade
    chmod 0664 /sys/class/power_supply/battery/wc_ic_chip_id
    chmod 0664 /sys/class/power_supply/battery/otp_firmware_ver_bin
    chmod 0664 /sys/class/power_supply/battery/otp_firmware_ver
    chmod 0664 /sys/class/power_supply/battery/tx_firmware_result
    chmod 0664 /sys/class/power_supply/battery/tx_firmware_ver
    chmod 0664 /sys/class/power_supply/battery/batt_tx_firmware
    chmod 0664 /sys/class/power_supply/battery/batt_hv_wireless_status
    chmod 0664 /sys/class/power_supply/battery/batt_hv_wireless_pad_ctrl
    chown system radio /sys/class/power_supply/battery/tx_firmware_result
    chown system radio /sys/class/power_supply/battery/batt_tune_chg_limit_cur
    chown system radio /sys/class/power_supply/battery/batt_tune_chg_temp_high
    chown system radio /sys/class/power_supply/battery/batt_tune_chg_temp_rec
    chown system radio /sys/class/power_supply/battery/batt_tune_coil_limit_cur
    chown system radio /sys/class/power_supply/battery/batt_tune_coil_temp_high
    chown system radio /sys/class/power_supply/battery/batt_tune_coil_temp_rec
    chown system radio /sys/class/power_supply/battery/batt_tune_wpc_temp_high
    chown system radio /sys/class/power_supply/battery/batt_tune_wpc_temp_high_rec
    chown system radio /sys/class/power_supply/battery/batt_tune_dchg_temp_high
    chown system radio /sys/class/power_supply/battery/batt_tune_dchg_temp_high_rec
    chown system radio /sys/class/power_supply/battery/batt_tune_dchg_batt_temp_high
    chown system radio /sys/class/power_supply/battery/batt_tune_dchg_batt_temp_high_rec
    chown system radio /sys/class/power_supply/battery/batt_tune_dchg_limit_input_cur
    chown system radio /sys/class/power_supply/battery/batt_tune_dchg_limit_chg_cur
    chown system radio /sys/class/power_supply/battery/batt_tune_fast_charge_current
    chown system radio /sys/class/power_supply/battery/batt_tune_float_voltage
    chown system radio /sys/class/power_supply/battery/batt_tune_input_charge_current
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_high_event
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_high_normal
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_high_rec_event
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_high_rec_normal
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_low_event
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_low_normal
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_low_rec_event
    chown system radio /sys/class/power_supply/battery/batt_tune_temp_low_rec_normal
    chown system radio /sys/class/power_supply/battery/batt_tune_ui_term_cur_1st
    chown system radio /sys/class/power_supply/battery/batt_tune_ui_term_cur_2nd
    chown system radio /sys/class/power_supply/battery/batt_misc_event
    chown system radio /sys/class/power_supply/battery/batt_tx_event
    chmod 0664 /sys/class/power_supply/battery/batt_tune_chg_limit_cur
    chmod 0664 /sys/class/power_supply/battery/batt_tune_chg_temp_high
    chmod 0664 /sys/class/power_supply/battery/batt_tune_chg_temp_rec
    chmod 0664 /sys/class/power_supply/battery/batt_tune_coil_limit_cur
    chmod 0664 /sys/class/power_supply/battery/batt_tune_coil_temp_high
    chmod 0664 /sys/class/power_supply/battery/batt_tune_coil_temp_rec
    chown 0664 /sys/class/power_supply/battery/batt_tune_wpc_temp_high
    chown 0664 /sys/class/power_supply/battery/batt_tune_wpc_temp_high_rec
    chmod 0664 /sys/class/power_supply/battery/batt_tune_dchg_temp_high
    chmod 0664 /sys/class/power_supply/battery/batt_tune_dchg_temp_high_rec
    chmod 0664 /sys/class/power_supply/battery/batt_tune_dchg_batt_temp_high
    chmod 0664 /sys/class/power_supply/battery/batt_tune_dchg_batt_temp_high_rec
    chmod 0664 /sys/class/power_supply/battery/batt_tune_dchg_limit_input_cur
    chmod 0664 /sys/class/power_supply/battery/batt_tune_dchg_limit_chg_cur
    chmod 0664 /sys/class/power_supply/battery/batt_tune_fast_charge_current
    chmod 0664 /sys/class/power_supply/battery/batt_tune_float_voltage
    chmod 0664 /sys/class/power_supply/battery/batt_tune_input_charge_current
    chmod 0664 /sys/class/power_supply/battery/batt_tune_temp_high_normal
    chmod 0664 /sys/class/power_supply/battery/batt_tune_temp_high_rec_normal
    chmod 0664 /sys/class/power_supply/battery/batt_tune_temp_low_normal
    chmod 0664 /sys/class/power_supply/battery/batt_tune_temp_low_rec_normal
    chmod 0664 /sys/class/power_supply/battery/batt_tune_ui_term_cur_1st
    chmod 0664 /sys/class/power_supply/battery/batt_tune_ui_term_cur_2nd
    chown system radio /sys/class/power_supply/battery/batt_tune_lrp_temp_high
    chmod 0664 /sys/class/power_supply/battery/batt_tune_lrp_temp_high
    chown system radio /sys/class/power_supply/battery/batt_tune_lrp_temp_rec
    chmod 0664 /sys/class/power_supply/battery/batt_tune_lrp_temp_rec
    chmod 0664 /sys/class/power_supply/battery/batt_misc_event
    chmod 0664 /sys/class/power_supply/battery/batt_tx_event
    chown system radio /sys/class/power_supply/battery/batt_filter_cfg
    chown system radio /sys/class/power_supply/battery/batt_current_ua_avg
    chown system radio /sys/class/power_supply/battery/batt_current_ua_now
    chown system radio /sys/class/power_supply/battery/voltage_avg
    chown system radio /sys/class/power_supply/battery/voltage_now
    chown system radio /sys/class/power_supply/battery/mode
    chown system radio /sys/class/power_supply/battery/safety_timer_set
    chown system radio /sys/class/power_supply/battery/safety_timer_info
    chown system radio /sys/class/power_supply/battery/batt_temp_test
    chown system radio /sys/class/power_supply/battery/vbus_value
    chown system radio /sys/class/power_supply/battery/dchg_temp
    chmod 0664 /sys/class/power_supply/battery/dchg_temp
    chown system radio /sys/class/power_supply/battery/dchg_temp_adc
    chmod 0664 /sys/class/power_supply/battery/dchg_temp_adc
    chown system radio /sys/class/power_supply/battery/blkt_temp
    chmod 0664 /sys/class/power_supply/battery/blkt_temp
    chown system radio /sys/class/power_supply/battery/blkt_temp_adc
    chmod 0664 /sys/class/power_supply/battery/blkt_temp_adc
    chown system radio /sys/class/power_supply/battery/dchg_adc_mode_ctrl
    chmod 0664 /sys/class/power_supply/battery/dchg_adc_mode_ctrl
    chown system radio /sys/class/power_supply/battery/batt_type
    chmod 0660 /sys/class/power_supply/battery/batt_type
    chown system system /efs/FactoryApp/cisd_data
    chmod 0660 /efs/FactoryApp/cisd_data
    chown system radio /sys/class/power_supply/battery/cisd_wc_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_wc_data_json
    chmod 0664 /sys/class/power_supply/battery/mode
    chown system radio /sys/class/power_supply/battery/wc_op_freq
    chmod 0664 /sys/class/power_supply/battery/wc_op_freq
    chown system radio /sys/class/power_supply/battery/wc_cmd_info
    chmod 0664 /sys/class/power_supply/battery/wc_cmd_info
    chown system radio /sys/class/power_supply/battery/error_cause
    chmod 0440 /sys/class/power_supply/battery/error_cause
    chown system radio /sys/class/power_supply/battery/batt_capacity_max
    chmod 0660 /sys/class/power_supply/battery/batt_capacity_max
    chown radio system /efs/Battery/batt_capacity_max
    chmod 0664 /efs/Battery/batt_capacity_max
    chown system radio /sys/class/power_supply/battery/batt_type
    chmod 0660 /sys/class/power_supply/battery/batt_type
    chown system radio /sys/class/power_supply/battery/cisd_fullcaprep_max
    chmod 0660 /sys/class/power_supply/battery/cisd_fullcaprep_max
    chown system radio /sys/class/power_supply/battery/cisd_wire_count
    chmod 0660 /sys/class/power_supply/battery/cisd_wire_count
    chown system radio /sys/class/power_supply/battery/cisd_data
    chmod 0660 /sys/class/power_supply/battery/cisd_data
    chown system system /efs/FactoryApp/cisd_data
    chmod 0660 /efs/FactoryApp/cisd_data
    chown system radio /sys/class/power_supply/battery/cisd_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_data_json
    chown system radio /sys/class/power_supply/battery/cisd_data_d_json
    chmod 0660 /sys/class/power_supply/battery/cisd_data_d_json
    chown system radio /sys/class/power_supply/battery/cisd_wc_data
    chmod 0660 /sys/class/power_supply/battery/cisd_wc_data
    chown system system /efs/FactoryApp/cisd_wc_data
    chmod 0660 /efs/FactoryApp/cisd_wc_data
    chown system radio /sys/class/power_supply/battery/cisd_wc_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_wc_data_json
    chown system system /efs/FactoryApp/sbm_log.log
    chmod 0664 /efs/FactoryApp/sbm_log.log
    chown system radio /sys/class/power_supply/battery/cisd_power_data
    chmod 0660 /sys/class/power_supply/battery/cisd_power_data
    chown system system /efs/FactoryApp/cisd_power_data
    chmod 0660 /efs/FactoryApp/cisd_power_data
    chown system radio /sys/class/power_supply/battery/cisd_power_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_power_data_json
    chown system radio /sys/class/power_supply/battery/cisd_pd_data
    chmod 0660 /sys/class/power_supply/battery/cisd_pd_data
    chown system system /efs/FactoryApp/cisd_pd_data
    chmod 0660 /efs/FactoryApp/cisd_pd_data
    chown system radio /sys/class/power_supply/battery/cisd_pd_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_pd_data_json
    chown system radio /sys/class/power_supply/battery/cisd_cable_data
    chmod 0660 /sys/class/power_supply/battery/cisd_cable_data
    chown system system /efs/FactoryApp/cisd_cable_data
    chmod 0660 /efs/FactoryApp/cisd_cable_data
    chown system radio /sys/class/power_supply/battery/cisd_cable_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_cable_data_json
    chown system radio /sys/class/power_supply/battery/cisd_tx_data
    chmod 0660 /sys/class/power_supply/battery/cisd_tx_data
    chown system system /efs/FactoryApp/cisd_tx_data
    chmod 0660 /efs/FactoryApp/cisd_tx_data
    chown system radio /sys/class/power_supply/battery/cisd_tx_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_tx_data_json
    chown system radio /sys/class/power_supply/battery/cisd_event_data
    chmod 0660 /sys/class/power_supply/battery/cisd_event_data
    chown system system /efs/FactoryApp/cisd_event_data
    chmod 0660 /efs/FactoryApp/cisd_event_data
    chown system radio /sys/class/power_supply/battery/cisd_event_data_json
    chmod 0660 /sys/class/power_supply/battery/cisd_event_data_json
    chown system system /sys/class/power_supply/battery/fg_asoc
    chmod 0660 /sys/class/power_supply/battery/fg_asoc
    chown system system /sys/class/power_supply/battery/fg_capacity
    chmod 0640 /sys/class/power_supply/battery/fg_capacity
    chown system system /sys/class/power_supply/battery/fg_fullcapnom
    chmod 0640 /sys/class/power_supply/battery/fg_fullcapnom
    chown system system /sys/class/power_supply/battery/batt_jig_gpio
    chmod 0664 /sys/class/power_supply/battery/batt_jig_gpio
    chown system radio /sys/class/power_supply/battery/wc_tx_en
    chmod 0664 /sys/class/power_supply/battery/wc_tx_en
    chown system system /dev/batt_misc
    chmod 0664 /dev/batt_misc
    chown system radio /sys/class/power_supply/battery/wc_tx_stop_capacity
    chmod 0660 /sys/class/power_supply/battery/wc_tx_stop_capacity
    chown system radio /sys/class/power_supply/battery/wc_tx_vout
    chmod 0664 /sys/class/power_supply/battery/wc_tx_vout
    chown system radio /sys/class/power_supply/battery/batt_tx_status
    chmod 0664 /sys/class/power_supply/battery/batt_tx_status
    chown system radio /sys/class/power_supply/battery/wc_rx_connected
    chmod 0660 /sys/class/power_supply/battery/wc_rx_connected
    chown system radio /sys/class/power_supply/battery/wc_tx_mfc_vin_from_uno
    chmod 0660 /sys/class/power_supply/battery/wc_tx_mfc_vin_from_uno
    chown system radio /sys/class/power_supply/battery/wc_tx_mfc_iin_from_uno
    chmod 0660 /sys/class/power_supply/battery/wc_tx_mfc_iin_from_uno
    chown system radio /sys/class/power_supply/battery/wc_tx_avg_curr
    chmod 0660 /sys/class/power_supply/battery/wc_tx_avg_curr
    chown system radio /sys/class/power_supply/battery/wc_tx_total_pwr
    chmod 0660 /sys/class/power_supply/battery/wc_tx_total_pwr
    chown system radio /sys/class/power_supply/battery/wc_tx_id
    chmod 0664 /sys/class/power_supply/battery/wc_tx_id
    chown system radio /sys/class/power_supply/battery/wc_auth_adt_sent
    chmod 0660 /sys/class/power_supply/battery/wc_auth_adt_sent
    chown system radio /sys/class/power_supply/battery/wc_duo_rx_power
    chmod 0660 /sys/class/power_supply/battery/wc_duo_rx_power
    chown system radio /sys/class/power_supply/battery/ext_event
    chmod 0660 /sys/class/power_supply/battery/ext_event
    chown system system /sys/class/power_supply/battery/batt_shipmode_test
    chmod 0660 /sys/class/power_supply/battery/batt_shipmode_test
    chown system system /sys/class/power_supply/battery/batt_misc_test
    chmod 0664 /sys/class/power_supply/battery/batt_misc_test
    chown system radio /sys/class/power_supply/battery/vbyp_voltage
    chmod 0660 /sys/class/power_supply/battery/vbyp_voltage
    chown system radio /sys/class/power_supply/battery/batt_main_voltage
    chmod 0660 /sys/class/power_supply/battery/batt_main_voltage
    chown system radio /sys/class/power_supply/battery/batt_sub_voltage
    chmod 0660 /sys/class/power_supply/battery/batt_sub_voltage
    chown system radio /sys/class/power_supply/battery/batt_main_current_ma
    chmod 0660 /sys/class/power_supply/battery/batt_main_current_ma
    chown system radio /sys/class/power_supply/battery/batt_sub_current_ma
    chmod 0660 /sys/class/power_supply/battery/batt_sub_current_ma
    chown system radio /sys/class/power_supply/battery/batt_main_con_det
    chmod 0660 /sys/class/power_supply/battery/batt_main_con_det
    chown system radio /sys/class/power_supply/battery/batt_sub_con_det
    chmod 0660 /sys/class/power_supply/battery/batt_sub_con_det
    chown system radio /sys/class/power_supply/battery/batt_main_enb
    chmod 0660 /sys/class/power_supply/battery/batt_main_enb
    chown system radio /sys/class/power_supply/battery/batt_sub_enb
    chmod 0660 /sys/class/power_supply/battery/batt_sub_enb
    chown system system /sys/class/power_supply/battery/ss_factory_mode
    chmod 0660 /sys/class/power_supply/battery/ss_factory_mode
    chown system system /sys/class/power_supply/battery/factory_charging_test
    chmod 0660 /sys/class/power_supply/battery/factory_charging_test
    chown system radio /sys/class/power_supply/battery/direct_charging_status
    chmod 0660 /sys/class/power_supply/battery/direct_charging_status
    chown system radio /sys/class/power_supply/battery/direct_charging_step
    chmod 0660 /sys/class/power_supply/battery/direct_charging_step
    chown system radio /sys/class/power_supply/battery/direct_charging_total_step
    chmod 0660 /sys/class/power_supply/battery/direct_charging_total_step
    chown system radio /sys/class/power_supply/battery/direct_charging_chg_status
    chmod 0660 /sys/class/power_supply/battery/direct_charging_chg_status
    chown system radio /sys/class/power_supply/battery/switch_charging_source
    chmod 0660 /sys/class/power_supply/battery/switch_charging_source
    chown system system /sys/class/power_supply/battery/dchg_adc_mode_ctrl
    chmod 0664 /sys/class/power_supply/battery/dchg_adc_mode_ctrl
    chown system radio /sys/class/power_supply/battery/pass_through
    chmod 0660 /sys/class/power_supply/battery/pass_through
    chown system radio /sys/class/power_supply/battery/boot_complete
    chmod 0660 /sys/class/power_supply/battery/boot_complete
    chown system radio /sys/class/power_supply/battery/batt_tune_wireless_vout_current
    chmod 0660 /sys/class/power_supply/battery/batt_tune_wireless_vout_current
    chown system radio /sys/class/power_supply/battery/pd_disable
    chmod 0664 /sys/class/power_supply/battery/pd_disable
    chown system radio /sys/class/power_supply/battery/wc_param_info
    chmod 0440 /sys/class/power_supply/battery/wc_param_info
    chown system radio /sys/class/power_supply/battery/batt_full_capacity
    chmod 0660 /sys/class/power_supply/battery/batt_full_capacity
    chown system system /efs/Battery/batt_full_capacity
    chmod 0660 /efs/Battery/batt_full_capacity
    chown system radio /sys/class/power_supply/battery/batt_after_manufactured
    chmod 0664 /sys/class/power_supply/battery/batt_after_manufactured
    chown system radio /sys/class/power_supply/wireless/sgf
    chmod 0666 /sys/class/power_supply/wireless/sgf
    chown system radio /sys/class/power_supply/battery/lrp
    chmod 0664 /sys/class/power_supply/battery/lrp
    chown system system /sys/class/power_supply/battery/battery_dump
    chmod 0660 /sys/class/power_supply/battery/battery_dump
    chown system radio /sys/class/power_supply/battery/dc_rb_en
    chmod 0660 /sys/class/power_supply/battery/dc_rb_en
    chown system radio /sys/class/power_supply/battery/dc_op_mode
    chmod 0660 /sys/class/power_supply/battery/dc_op_mode
    chown system radio /sys/class/power_supply/battery/dc_adc_mode
    chmod 0660 /sys/class/power_supply/battery/dc_adc_mode
    chown system radio /sys/class/power_supply/battery/dc_vbus
    chmod 0660 /sys/class/power_supply/battery/dc_vbus

# If AP debug_level is low, recovery mode for ALL peripheral will be endabled
on post-fs-data && property:ro.boot.debug_level=0x4f4c
   setprop persist.vendor.ssr.restart_level ALL_ENABLE

# Enable recovery mode for modem only (CP Ramdump OFF && dbg_level MID)
on post-fs-data && property:ro.boot.cp_debug_level=0x55FF && property:ro.boot.debug_level=0x494d
   setprop persist.vendor.ssr.restart_level mss

# Enable recovery mode for modem only (CP Ramdump OFF && dbg_level HI)
on post-fs-data && property:ro.boot.cp_debug_level=0x55FF && property:ro.boot.debug_level=0x4948
   setprop persist.vendor.ssr.restart_level mss

# Disable recovery mode for modem only (CP Ramdump ON && dbg_level MID)
on post-fs-data && property:ro.boot.cp_debug_level=0x5500 && property:ro.boot.debug_level=0x494d
   setprop persist.vendor.ssr.restart_level ALL_DISABLE

# Disable recovery mode for modem only (CP Ramdump ON && dbg_level HI)
on post-fs-data && property:ro.boot.cp_debug_level=0x5500 && property:ro.boot.debug_level=0x4948
   setprop persist.vendor.ssr.restart_level ALL_DISABLE

on early-boot

on boot


